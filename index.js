/**
 * Module dependencies.
 */

var express = require('express')
  , request = require('request')
  // , key = require('./room-barge-firebase-key.json')
  , awsIot = require('aws-iot-device-sdk')
  // , firebase = require('firebase-admin')
  , bodyParser = require('body-parser')
  , port = process.env.PORT || 3000
  , _ = require('lodash')
  , MongoProvider = require('./mongo').MongoProvider;

// firebase.initializeApp({
//   credential: firebase.credential.cert(key),
//   databaseURL: "https://room-barge.firebaseio.com"
// });
// var database = firebase.database();
var app = express();
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json());

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

var NODE_ID = 'PIR-SENSOR_NODE'; 

var mongoProvider = new MongoProvider('ds121730.mlab.com', 21730, function(){
	app.listen(port, function() {
	  // console.log("Node app is running at localhost:" + port);
	});
});

var device = awsIot.device({ 
  keyPath: './certs/b9f426ae6f-private.pem.key', 
  certPath: './certs/b9f426ae6f-certificate.pem.crt', 
  caPath: './certs/RootCA-VeriSign.pem', 
  clientId: NODE_ID, 
  host: 'a2cropnrbxc8tt.iot.us-east-1.amazonaws.com', 
  port: 8883, 
  region: 'us-east-1', 
  debug: false, // optional to see logs on console
}); 

device.on('connect', function() { 
  // console.log('device connected!'); 
  device.subscribe('$aws/things/PIR-SENSOR_NODE/shadow/get/accepted'); 
  device.subscribe('$aws/things/PIR-SENSOR_NODE/shadow/get/rejected'); 
  // Publish an empty packet to topic `$aws/things/PIR-SENSOR_NODE/shadow/get` 
  // to get the latest shadow data on either `accepted` or `rejected` topic 
  device.publish('$aws/things/PIR-SENSOR_NODE/shadow/get', ''); 
  // device.publish('$aws/things/turnoff/shadow/update', JSON.stringify({
  //       'state': {
  //           'reported': {
  //               'actual_pool_temp': 20 + Math.random() * 10
  //           }
  //       }
  //   }));
}); 
 
device.on('message', function(topic, payload) { 
	payload = JSON.parse(payload.toString()); 
	// console.log('message from ', topic, payload);
	device.publish('xyz', JSON.stringify({
        'state': {
            'reported': {
                'actual_pool_temp': 20 + Math.random() * 10
            }
        }
    }));
	payload.state.desired.data = (new Date(payload.state.desired.data)).getTime();
	addAnalysticData(payload.state.desired.macId, payload.state.desired.data);
});  

function addAnalysticData(macId, data){
	mongoProvider.findByObj('analytics',{
	    macId: macId,
	    dateTime: data
	}, function( error, docs) {
	    // if (error) console.log('failed to fetch the analytic data of: ', macId, data);
	    // else console.log('successfully fetched: ', docs);
	    if(!(docs && docs.length)){
		    mongoProvider.save('analytics', {
			    macId: macId,
			    dateTime: data
			}, function( error, docs) {
			    // if (error) console.log('failed to save the analytic data of: ', macId, data);
			});
	    }

	})
}

app.get('/getAllData', function(req, res){
	mongoProvider.findAll('deviceRoom', function(error, roomData){
		mongoProvider.findAll('analytics', function(error, docs){
		    // console.log('retervied Data: ', docs.length);
		    if(error){
				res.send({err: error});
			}else {
				var data = {};
				_.map(docs, function(v){
					var roomName = (_.filter(roomData, function(val){ return val.macId == v.macId}))[0];
					roomName = roomName ? roomName.roomName : "Dummy Room";
					if(data[v.macId]){
						data[v.macId].y++;
					}else{
						data[v.macId] = {name: roomName, y: 1};
					}
				});
			    res.send({success: true, data: Object.values(data)});
			}
		});
	});
});

app.post('/getMacData', function(req, res){
	mongoProvider.findByObj('analytics', {macId: req.body.macId}, function(error, docs){
	    // console.log('retervied Data: ', docs.length);
	    res.send({err: error, success: !!error, data: docs});
	});
});

app.post('/addNewDevice', function(req, res){
	mongoProvider.save('deviceRoom', {macId: req.body.macId, roomName: req.body.roomName}, function(err, doc){
		if(err){
			res.send({err: error});
		}else {
			mongoProvider.save('analytics', {macId: req.body.macId, dateTime: new Date()}, function(err, doc){
				if(err){
					res.send({err: error});
				}else {
					res.send({success: true});
				}
			});
		}
	})
});

app.get('/roomList', function(req, res){
	mongoProvider.findAll('deviceRoom', function(err, docs){
		if(err){
			res.send({err: error});
		}else {
			res.send({success: true, roomList: docs});
		}
	})
});
