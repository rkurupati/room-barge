var MongoClient = require('mongodb').MongoClient;
var Connection = require('mongodb').Connection;
var Server = require('mongodb').Server;
var BSON = require('mongodb').BSON;
var ObjectID = require('mongodb').ObjectID;
var dbObj;

MongoProvider = function(host, port, cb) {
  // this.db = new Db('iot-allout', new Server(host, port, {safe: false}, {auto_reconnect: true}, {}));
  // this.db.open(function(){});
  // var mongoclient = new MongoClient(new Server(host, port), {native_parser: true});

  // Open the connection to the server
  // mongoclient.open(function(err, mongoclient) {

  //   // Get the first db and do an update document on it
  //   that.db = mongoclient.db("iot-allout");
  //   db.collection('mongoclient_test').update({a:1}, {b:1}, {upsert:true}, function(err, result) {
  //     assert.equal(null, err);
  //     assert.equal(1, result);

  //     // Get another db and do an update document on it
  //     var db2 = mongoclient.db("integration_tests2");
  //     db2.collection('mongoclient_test').update({a:1}, {b:1}, {upsert:true}, function(err, result) {
  //       assert.equal(null, err);
  //       assert.equal(1, result);

  //       // Close the connection
  //       mongoclient.close();
  //     });
  //   });
  // });
  MongoClient.connect("mongodb://admin:admin@"+host+":"+port+"/iot-allout", function(err, mongoclient){
    // if(err) console.log('DB not connected: ', err);
    // else console.log("DB is connected: ");
    dbObj = mongoclient.db('iot-allout');
    cb();
  })
};


MongoProvider.prototype.getCollection= function(collection, callback) {
  // collection = collection || 'analytics';
  dbObj.collection(collection, function(error, analytic_collection) {
    if( error ) callback(error);
    else callback(null, analytic_collection);
  });
};

//find all analytics
MongoProvider.prototype.findAll = function(collection, callback) {
    this.getCollection(collection, function(error, analytic_collection) {
      if( error ) callback(error)
      else {
        analytic_collection.find().toArray(function(error, results) {
          if( error ) callback(error)
          else callback(null, results)
        });
      }
    });
};

//find an analytic by ID
MongoProvider.prototype.findById = function(collection, id, callback) {
    this.getCollection(collection, function(error, analytic_collection) {
      if( error ) callback(error)
      else {
        analytic_collection.findOne({_id: analytic_collection.db.bson_serializer.ObjectID.createFromHexString(id)}, function(error, result) {
          if( error ) callback(error)
          else callback(null, result)
        });
      }
    });
};

//find an analytic by Obj
MongoProvider.prototype.findByObj = function(collection, queryObj, callback) {
    this.getCollection(collection, function(error, analytic_collection) {
      if( error ) callback(error)
      else {
        analytic_collection.find(queryObj).toArray(function(error, result) {
          if( error ) callback(error)
          else callback(null, result)
        });
      }
    });
};

//find by objects in sort order
// ==============================================
MongoProvider.prototype.findSortedObjects = function(collection, query, sortQuery, callback) {
  this.getCollection(collection, function(error, _collection) {
    if( error ) callback(error)
    else {
      _collection.find(query).sort(sortQuery).toArray(function(error, result) {
        if( error ) callback(error)
        else callback(null, result)
      });
    }
  });
};


//save new analytic
MongoProvider.prototype.save = function(collection, analytics, callback) {
    this.getCollection(collection, function(error, analytic_collection) {
      if( error ) callback(error)
      else {
        if( typeof(analytics.length)=="undefined")
          analytics = [analytics];

        for( var i =0;i< analytics.length;i++ ) {
          analytic = analytics[i];
          analytic.created_at = new Date();
        }

        analytic_collection.insert(analytics, function() {
          callback(null, analytics);
        });
      }
    });
};

// update an analytic
MongoProvider.prototype.update = function(collection, analyticId, analytics, callback) {
    this.getCollection(collection, function(error, analytic_collection) {
      if( error ) callback(error);
      else {
        analytic_collection.update(
					{_id: analytic_collection.db.bson_serializer.ObjectID.createFromHexString(analyticId)},
					analytics,
					function(error, analytics) {
						if(error) callback(error);
						else callback(null, analytics)       
					});
      }
    });
};

//delete analytic
MongoProvider.prototype.delete = function(collection, analyticId, callback) {
	this.getCollection(collection, function(error, analytic_collection) {
		if(error) callback(error);
		else {
			analytic_collection.remove(
				{_id: analytic_collection.db.bson_serializer.ObjectID.createFromHexString(analyticId)},
				function(error, analytic){
					if(error) callback(error);
					else callback(null, analytic)
				});
			}
	});
};

exports.MongoProvider = MongoProvider;